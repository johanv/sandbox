# Johanv\Sandbox

This repository contains some classes to illustrate various aspects of inheritance.

## Install php dependencies

```
composer install
```

## Run the unit tests:

``` 
./vendor/bin/phpunit tests
```

## Run phpstan to check for correct generics

```
./vendor/bin/phpstan analyse --level=max src/ tests/
```
