<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

abstract class Animal implements Talking
{
    public final function eat(): string
    {
        return "nom nom nom";
    }

    public final function sleep(): string
    {
        return "zzzz";
    }

    public abstract function talk(): string;
}