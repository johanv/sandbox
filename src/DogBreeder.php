<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

final class DogBreeder implements AnimalBreeder
{
    public function getAnimal(): Dog
    {
        return new Dog();
    }
}