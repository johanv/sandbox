<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

final class Ear extends AnimalListener
{
    public function listenTo(Talking ...$animals): string
    {
        return 'They said: ' . implode(
                ' ',
                array_map(
                    fn(Talking $talking) => $talking->talk(),
                    $animals
                )
            );
    }
}