<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

final class Dog extends Animal
{
    public function talk(): string
    {
        return "bark!";
    }
}