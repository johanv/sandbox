<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

class CatFlap
{
    public function letCatIn(Cat $cat): string
    {
        // some functionality that depends on the expected behavior of a cat
        $result = $cat->talk();
        if ($result === 'meow!') {
            return 'Cat entered.';
        }

        throw new \Exception('Whoa, this cat is acting weird 😱');
    }
}