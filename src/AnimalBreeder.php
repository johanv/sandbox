<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

interface AnimalBreeder
{
    public function getAnimal(): Animal;
}