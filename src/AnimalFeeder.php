<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

/**
 * @template TAnimal of Animal
 */
abstract class AnimalFeeder
{
    /**
     * @param TAnimal ...$animals
     */
    public function feed(Animal ...$animals): string
    {
        $count = 0;

        foreach ($animals as $animal) {
            $result = $animal->eat();
            if ($result === 'nom nom nom') {
                ++$count;
            } else {
                throw new \Exception('Oh no, could not feed this animal 😱');
            }
        }

        return "Fed {$count} animal(s).";
    }
}