<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

interface Talking
{
    public function talk(): string;
}