<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

class AnimalListener
{
    public function listenTo(Animal ...$animals): string
    {
        return 'They said: ' . implode(
            ' ',
            array_map(
                fn(Animal $animal) => $animal->talk(),
                $animals
            )
        );
    }
}