<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

final class Cat extends Animal
{
    public function talk(): string
    {
        return "meow!";
    }
}