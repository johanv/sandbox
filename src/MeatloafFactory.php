<?php

declare(strict_types=1);

namespace Johanv\Sandbox;

final class MeatloafFactory
{
    private AnimalBreeder $animalBreeder;

    public function __construct(AnimalBreeder $animalBreeder) {
        $this->animalBreeder = $animalBreeder;
    }

    public function getMeatloaf(): string
    {
        $animal = $this->animalBreeder->getAnimal();

        return 'A nice meatloaf made of ' . get_class($animal);
    }
}