<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\Cat;
use PHPUnit\Framework\TestCase;

final class CatTest extends TestCase
{
    /** @test */
    public function itEats(): void
    {
        $cat = new Cat();

        $actual = $cat->eat();
        $expected = 'nom nom nom';

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itSleeps(): void
    {
        $cat = new Cat();

        $actual = $cat->sleep();
        $expected = "zzzz";

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itMeows(): void
    {
        $cat = new Cat();

        $actual = $cat->talk();
        $expected = "meow!";

        $this->assertEquals($expected, $actual);
    }
}