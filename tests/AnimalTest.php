<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\Animal;
use PHPUnit\Framework\TestCase;

final class AnimalTest extends TestCase
{
    /** @test */
    public function itEats(): void
    {
        $animal = new class extends Animal {
            public function talk(): string
            {
                return "meh";
            }
        };

        $actual = $animal->eat();
        $expected = 'nom nom nom';

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itSleeps(): void
    {
        $animal = new class extends Animal {
            public function talk(): string
            {
                return "meh!";
            }
        };

        $actual = $animal->sleep();
        $expected = "zzzz";

        $this->assertEquals($expected, $actual);
    }
}