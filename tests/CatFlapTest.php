<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\Cat;
use Johanv\Sandbox\CatFlap;
use Johanv\Sandbox\Dog;
use PHPUnit\Framework\TestCase;

final class CatFlapTest extends TestCase
{
    /**
     * @test
     * @dataProvider catProvider
     */
    public function itLetsCatIn(Cat $cat): void
    {
        $catFlap = new CatFlap();

        $actual = $catFlap->letCatIn($cat);
        $expected = "Cat entered.";

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return Cat[][]
     */
    public function catProvider(): array
    {
        return [
            ['cat' => new Cat()],
            ['cat' => new Cat()],
        ];
    }
}