<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\DogBreeder;
use Johanv\Sandbox\MeatloafFactory;
use PHPUnit\Framework\TestCase;

final class MeatloafFactoryTest extends TestCase
{
    /** @test */
    public function itMakesMeatLoafFromDogs(): void
    {
        $factory = new MeatloafFactory(new DogBreeder());
        $actual = $factory->getMeatloaf();
        $expected = 'A nice meatloaf made of Johanv\Sandbox\Dog';

        $this->assertEquals($expected, $actual);
    }
}