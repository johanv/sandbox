<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\AnimalListener;
use Johanv\Sandbox\Cat;
use Johanv\Sandbox\Dog;
use PHPUnit\Framework\TestCase;

final class AnimalListenerTest extends TestCase
{
    /** @test */
    public function itListensToVariousAnimals(): void
    {
        $listener = new AnimalListener();

        $actual = $listener->listenTo(new Cat(), new Cat(), new Dog());
        $expected = 'They said: meow! meow! bark!';

        $this->assertEquals($expected, $actual);
    }
}