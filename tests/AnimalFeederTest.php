<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\AnimalFeeder;
use Johanv\Sandbox\Cat;
use Johanv\Sandbox\Dog;
use PHPUnit\Framework\TestCase;

final class AnimalFeederTest extends TestCase
{
    /** @test */
    public function itFeedsVariousAnimals(): void
    {
        $feeder = new class extends AnimalFeeder{};

        $actual = $feeder->feed(
            new Cat(),
            new Dog(),
            new Dog()
        );
        $expected = "Fed 3 animal(s).";

        $this->assertEquals($expected, $actual);
    }
}