<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\Dog;
use PHPUnit\Framework\TestCase;

final class DogTest extends TestCase
{
    /** @test */
    public function itEats(): void
    {
        $dog = new Dog();

        $actual = $dog->eat();
        $expected = 'nom nom nom';

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itSleeps(): void
    {
        $dog = new Dog();

        $actual = $dog->sleep();
        $expected = "zzzz";

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itBarks(): void
    {
        $dog = new Dog();

        $actual = $dog->talk();
        $expected = "bark!";

        $this->assertEquals($expected, $actual);
    }
}