<?php

declare(strict_types=1);

namespace Johanv\Sandbox\Tests;

use Johanv\Sandbox\Cat;
use Johanv\Sandbox\CatFeeder;
use PHPUnit\Framework\TestCase;

final class CatFeederTest extends TestCase
{
    /** @test */
    public function itFeedsACat(): void
    {
        $catFeeder = new CatFeeder();
        $actual = $catFeeder->feed(new Cat());
        $expected = 'Fed 1 animal(s).';

        $this->assertEquals($expected, $actual);
    }
}